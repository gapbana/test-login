const Router = require('koa-router')
const login = require('./login');

const router = new Router()

router.post("api/login",login.postHandler)


module.exports = router.routes();