import React, { Component } from "react";
import { Row, FormGroup, FormControl,  Button, HelpBlock } from 'react-bootstrap';
import './login.css';
import { isEmail, isEmpty, isLength, isContainWhiteSpace } from 'shared/validator';
import logo from './logo.svg';
//import serv from '../../../server/index';

class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            formData: {}, 
            errors: {},
            formSubmitted: false,
            loading: false
        }
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        let { formData } = this.state;
        formData[name] = value;

        this.setState({
            formData: formData
        });
    }

    validateLoginForm = (e) => {
        
        let errors = {};
        const { formData } = this.state;

        if (isEmpty(formData.email)) {
            errors.email = "Email can't be blank";
        } else if (!isEmail(formData.email)) {
            errors.email = "Please enter a valid email";
        }

        if (isEmpty(formData.password)) {
            errors.password = "Password can't be blank";
        }  else if (isContainWhiteSpace(formData.password)) {
            errors.password = "Password should not contain white spaces";
        } else if (!isLength(formData.password, { gte: 6, lte: 16, trim: true })) {
            errors.password = "Password's length must between 6 to 16";
        }

        if (isEmpty(errors)) {
            return true;
        } else {
            return errors;
        }    
    }

    login = (e) => {
        const { formData } = this.state;
        const EMAIL = 'example@appman.co.th';
        const PASSWORD = 'password';
        e.preventDefault();

        let errors = this.validateLoginForm();

        if(errors === true){
            if (formData.email === EMAIL && formData.password === PASSWORD) {
                alert("OK");
            window.location.reload() 
            } else {
                alert("email or password may be wrong");
            window.location.reload() 
            }
              
        } else {
            this.setState({
                errors: errors,
                formSubmitted: true
            });
        }
    }

    render() {

        const { errors, formSubmitted } = this.state;

        return (
            <div className="Login">
                <Row>
                    <form onSubmit={this.login}>
                    <img src={logo} className="App-logo" alt="logo" />
                        <FormGroup controlId="email" validationState={ formSubmitted ? (errors.email ? 'error' : 'success') : null }>
                            <p>E-mail Address</p>
                            <FormControl type="text" className="Inputform" name="email" placeholder="example@mail.com" onChange={this.handleInputChange} />
                        { errors.email && 
                            <HelpBlock>{errors.email}</HelpBlock> 
                        }
                        </FormGroup >
                        <FormGroup controlId="password" validationState={ formSubmitted ? (errors.password ? 'error' : 'success') : null }>
                            <p>Password</p>
                            <FormControl type="password" className="Inputform" name="password" placeholder="your password..." onChange={this.handleInputChange} />
                        { errors.password && 
                            <HelpBlock>{errors.password}</HelpBlock> 
                        }
                        </FormGroup>
                        <Button type="submit" className="btn-signin" >SIGN IN</Button>
                        <br></br>
                        <span>
                        <a className="forget">Forget password ?</a>
                        <a className="create">create a new account</a>
                        </span>
                        
                    </form>
                </Row>
            </div>
        )
    }
}

export default Login;